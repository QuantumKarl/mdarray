# README #

### What is it? ###
It is a container that allows access of data in multiple dimensions, thus the name Multi Dimensional Array.

### Why does it exist? ###
With C style arrays and stl containers, you can access data using multiple dimensions (via the overloaded operators). However, this feature is NOT possible at run time / dynamically.
This was a feature I required for my [Seki_GO](https://bitbucket.org/QuantumKarl/seki_go/src) (I wanted the ability for the user to choose the board dimensions).

### How do I use it? ###
See src/MDArrayTest.hpp this file contains various tests / example uses.

### Is it fast? ###
Compared with the compile time code of C styl arrays and stl containers, No. It requires some calculations to generate the correct index.
However, you can iterate MDArray linearly which has no speed penalties / additional calculations.

### How do I get set up? ###
Use the **make file** (GCC) or the **MSVS 2015 sln**.

### Does it have any dependencies? ###

The tests have optional dependencies on (toggle via #define in main.cpp):

* [Boost](http://www.boost.org)

### Is it memory safe? ###
Yes, it has been tested with:

* [CppCheck](http://cppcheck.sourceforge.net).
* [Valgrind](http://valgrind.org/docs/manual/mc-manual.html).
* [Callgrind](http://valgrind.org/docs/manual/cl-manual.html).

No errors or problems detected.

### What is next? ###
Perhaps further testing with non pod data types such as std::string.