/*	This file is part of MDArray.
	Copyright(C) 2016 by Karl Wesley Hutchinson

	MDArray is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	MDArray is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MDArray. If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined( D_MD_ARRAY_HPP )
#define D_MD_ARRAY_HPP

#include <assert.h>

#include <utility>
#include <iterator>
#include <algorithm>
#include <iostream>
using namespace std;

namespace sys_templates
{
	// TEMPLATE STRUCT _Param_tester
	template<class...>
	struct param_tester
	{	// test if parameters are valid
		typedef void type;
	};

	// ALIAS TEMPLATE void_t
	template<class... _Types>	// TRANSITION, needs CWG 1558
	using void_t = typename param_tester<_Types...>::type;

	// TEMPLATE CLASS is_iterator
	template<class T,
		class = void>
		struct is_iterator
		: false_type
	{	// default definition
	};

	template<class T>
	struct is_iterator<T, void_t<
		typename iterator_traits<T>::iterator_category
	> >
		: true_type
	{	// defined if iterator_category is provided by iterator_traits<_Ty>
	};
}

//--------------------------------------------------------------------------------------------------------------------------------
/* Multi Dimensional Array
	An array that is capable of accessing data in whatever dimension the user pleases.
	The data is stored in a single dimensional array (row major order).

	For example:
	{
		MDArray<float> data2d({2, 2});			// 2D, with x:2, y:2
		MDArray<float> data3d({2, 3, 4});		// 3D, with x:2, y:3, z:4
		MDArray<float> data4d({4, 4, 6, 5});	// 4D, with x:4, y:4, z:6, w:5

		data2d.At( {0, 1} ) = 10.0f; // sets the data at x:0, y:1 to 10.0f

		size_t coord[4] = { 2,0,2,0 };
		float val = data4d.At( begin(coord), end(coord) ); // gets the value at {2, 0, 2, 0}
	}

	WARNING: A dimension of length 0 is invalid.
	WARNING: A dimension of length 1 is pretty useless, but is supported.
	WARNING: These functions are strategically inlined to avoid branching functions / to make all function leaf functions.
	NOTE: You can provide your own allocators via the template arguments.
	NOTE: If you want to give this container custom allocators which have an instance,
		use the default ctor to pass your instance to the container,
		then call the init function with your desired dimensions.
*/
template< typename data_t, typename data_alloc_t = allocator<data_t>, typename dim_t = size_t, typename dim_alloc_t = allocator<dim_t> >
class MDArray final
{
public:
	//-------------------------------- Public aliases --------------------------------
	using container_t		= vector<data_t, data_alloc_t >;
	using dim_container_t	= vector<dim_t, dim_alloc_t >;

	using this_type = MDArray<data_t, data_alloc_t, dim_t, dim_alloc_t>;

	using data_allocator_type		= data_alloc_t;
	using dimension_allocator_type	= dim_alloc_t;

	using value_type		= typename container_t::value_type;
	using size_type			= typename container_t::size_type;
	using difference_type	= typename container_t::difference_type;
	using pointer			= typename container_t::pointer;
	using const_pointer		= typename container_t::const_pointer;
	using reference			= typename container_t::reference;
	using const_reference	= typename container_t::const_reference;

	using iterator			= typename container_t::iterator;
	using const_iterator	= typename container_t::const_iterator;

	using reverse_iterator			= typename container_t::reverse_iterator;
	using const_reverse_iterator	= typename container_t::reverse_iterator;

	//-------------------------------- Constructors --------------------------------

	//--------------------------------
	/* MDArray()
		default ctor, only means to pass allocator instances to the internal containers.
	*/
	MDArray( data_alloc_t const& i_data_alloc = data_alloc_t(), dim_alloc_t const& i_dim_alloc = dim_alloc_t() ) noexcept
		:
		m_array( i_data_alloc ),
		m_dim( i_dim_alloc )
	{ }

	//--------------------------------
	/* MDArray( i_first, i_last )
		most usual / main ctor.
		i_first should point to the first element in a container containing, the dimensions of the container.
		i_last should point ot the end element in the same container.
	*/
	template<class itr_t, class = typename enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	MDArray( itr_t i_first, itr_t i_last )
		:
		m_array(),
		m_dim()
	{
		init( i_first, i_last );
	}

	//--------------------------------
	/* MDArray( i_initList )
	*/
	MDArray( initializer_list<dim_t> i_initList )
		:
		m_array(),
		m_dim()
	{
		assert( i_initList.size() > 0 );
		init( std::begin( i_initList ), std::end( i_initList ) );
	}

	//--------------------------------
	/* MDArray( i_other )
		copy ctor
	*/
	explicit MDArray( this_type const& i_other ) noexcept
		:
		m_array( i_other.m_array ),
		m_dim( i_other.m_dim )
	{ }

	//--------------------------------
	/* MDArray( i_other )
		move copy ctor
	*/
	explicit MDArray( this_type&& i_other ) noexcept
		:
		m_array( move( i_other.m_array ) ),
		m_dim( move( i_other.m_dim ) )
	{ }

	//--------------------------------
	/* ~MDArray()
		dtor, nothing todo

		NOTE: NOT virtual.
	*/
	~MDArray() noexcept
	{ }

	//-------------------------------- Operators --------------------------------

	//--------------------------------
	/* operator=( i_rhs )
		copy assignment
	*/
	this_type& operator=( this_type const& i_rhs ) noexcept
	{
		m_array = i_rhs.m_array;
		m_dim = i_rhs.m_dim;
		return *this;
	}

	//--------------------------------
	/* operator=( i_rhs )
		copy move
	*/
	this_type& operator=( this_type&& i_rhs ) noexcept
	{
		m_array = move( i_rhs.m_array );
		m_dim = move( i_rhs.m_dim );
		return *this;
	}

	//-------------------------------- Public functions --------------------------------

	//--------------------------------
	/* at( i_n )
		index into the container.

		NOTE: unlike std::at this only provides bounds checking via asserts.
	*/
	inline data_t& at( size_t const i_n )
	{
		assert( m_array.size() > 0 );
		assert( i_n < m_array.size() );

		return m_array[i_n];
	}

	//--------------------------------
	/* at( i_n ) const
		index into the container.

		NOTE: unlike std::at this only provides bounds checking via asserts.
	*/
	inline data_t const& at( size_t const i_n ) const
	{
		assert( m_array.size() > 0 );
		assert( i_n < m_array.size() );

		return m_array[i_n];
	}

	//--------------------------------
	/* at( i_first, i_last )
		index into the container via multidimensional co-ordinates.

		NOTE: unlike std::at this only provides bounds checking via asserts.
	*/
	template<class itr_t, class = typename enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	data_t& at( itr_t i_first, itr_t i_last )
	{
		assert( static_cast<size_t>( distance( i_first, i_last ) ) == m_dim.size() );

		size_t index = 0;
		bool const res = decode_index( i_first, i_last, index );

		assert( res == true && index < m_array.size() );
		return m_array[index];
	}

	//--------------------------------
	/* at( i_first, i_last ) const
		index into the container via multidimensional co-ordinates.

		NOTE: unlike std::at this only provides bounds checking via asserts.
	*/
	template<class itr_t, class = typename enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	data_t const& at( itr_t i_first, itr_t i_last ) const
	{
		assert( static_cast<size_t>( distance( i_first, i_last ) ) == m_dim.size() );

		size_t index = 0;
		bool res = decode_index( i_first, i_last, index );

		assert( res == true && index < m_array.size() );
		return m_array[index];
	}

	//--------------------------------
	/* at( i_initList )
		index into the container via multidimensional co-ordinates (given in a initializer list).

		NOTE: unlike std::at this only provides bounds checking via asserts.
	*/
	inline data_t& at( initializer_list<dim_t> i_initList )
	{
		assert( i_initList.size() > 0 );
		return at( i_initList.begin(), i_initList.end() );
	}

	//--------------------------------
	/* at( i_initList ) const
		index into the container via multidimensional co-ordinates (given in a initializer list).

		NOTE: unlike std::at this only provides bounds checking via asserts.
	*/
	inline data_t const& at( initializer_list<dim_t> i_initList ) const
	{
		assert( i_initList.size() > 0 );
		return at( i_initList.cbegin(), i_initList.cend() );
	}

	//--------------------------------
	/* begin()
		get iterator to first element.
	*/
	inline auto begin()
	{
		assert( m_array.size() > 0 );
		return std::begin( m_array );
	}

	//--------------------------------
	/* begin() const
		get iterator to first element.
	*/
	inline auto begin() const
	{
		assert( m_array.size() > 0 );
		return std::cbegin( m_array );
	}

	//--------------------------------
	/* cbegin() const
		get iterator to first element.
	*/
	inline auto cbegin() const
	{
		assert( m_array.size() > 0 );
		return std::cbegin( m_array );
	}

	//--------------------------------
	/* cend() const
		get an iterator to one element past the end of the container.
	*/
	inline auto cend() const
	{
		assert( m_array.size() > 0 );
		return std::cend( m_array );
	}

	//--------------------------------
	/* clear()
		unlike STL container's clear, this function resets all contents to zero.

		NOTE: leaves size intact since this container is intended to parallel an array.
	*/
	void clear()
	{
		size_t const size = m_array.size();
		m_array.resize( 0 );
		m_array.resize( size, data_t( 0 ) );
	}

	//--------------------------------
	/* crbegin() const
		get iterator to first element.
	*/
	inline auto crbegin() const
	{
		assert( m_array.size() > 0 );
		return std::crbegin( m_array );
	}

	//--------------------------------
	/* crend() const
		get an iterator to one element past the end of the container.
	*/
	inline auto crend() const
	{
		assert( m_array.size() > 0 );
		return std::crend( m_array );
	}

	//--------------------------------
	/* debug_cout() const
		cout the contents of a 2D container.

		NOTE: created for debugging.
	*/
	void debug_cout() const
	{
		if( m_dim.size() != 2 )
		{
			cout << "MDArray::debug_count only supports 2D arrays, unable to cout contents" << endl;
			return;
		}

		cout << endl;
		size_t arr[2] = { 0,0 };
		size_t index = 0;
		for( size_t i = m_dim[1] - 1; i < m_dim[1]; --i ) // y
		{
			arr[1] = i;
			for( size_t j = 0; j < m_dim[0]; ++j ) // x
			{
				arr[0] = j;
				decode_index( arr, &arr[2], index );
				cout << static_cast<data_t>( m_array[index] ) << ",";
			}
			cout << endl;
		}
	}

	//--------------------------------
	/* decode_index( i_first, i_last, o_index ) const
		given multidimensional co-ordinates (encoded co-ordinates) via iterators, decode them into a one dimensional index.

		i.e. convert x: 2, y: 2 to its 1D form.
	*/
	template<class itr_t, class = typename enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	inline bool decode_index( itr_t i_first, itr_t i_last, size_t& o_index ) const
	{
		assert( static_cast<size_t>( distance( i_first, i_last ) ) == m_dim.size() );
		assert( m_array.size() > 0 );
		assert( m_dim.empty() == false );

		auto index = *i_first;
		auto itr = std::begin( m_dim );
		auto dimProd = *itr;

		if( index < *itr )
		{
			++i_first;
			++itr;
			auto endItr = std::end( m_dim );
			do
			{
				auto& coord = *i_first;
				auto& dim = *itr;

				if( coord < dim )
				{
					index += dimProd * coord;
					dimProd *= dim;

					++i_first;
					++itr;
				}
				else
				{
					return false;
				}
			} while( itr != endItr && i_first != i_last );

			o_index = index;
			return true;
		}

		return false;
	}

	//--------------------------------
	/* dimensions() const
		get a const reference to the internal container which contains the dimension values.
	*/
	auto const& dimensions() const
	{
		assert( m_dim.empty() == false );
		return m_dim;
	}

	//--------------------------------
	/* encode_index( i_index, o_first, o_last )
		given an index, encode it into its multidimensional form.

		i.e. convert 26 to x: 2, y: 2, z: 2 (on a 3x3x3 array)
	*/
	template<class itr_t, class = typename enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	inline bool encode_index( size_t const i_index, itr_t o_first, itr_t o_last ) const
	{
		assert( static_cast<size_t>( distance( o_first, o_last ) ) == m_dim.size() );
		assert( m_array.size() > 0 );
		assert( m_dim.size() >= 2 );

		if( i_index >= m_array.size() ) return false;

		auto dimItr = std::begin( m_dim );
		auto itrEnd = dimItr + ( m_dim.size() - 1 );

		auto outputItr = o_first;
		( *outputItr ) = i_index % ( *dimItr );
		auto dimProduct = *dimItr;

		++dimItr;
		++outputItr;
		if( dimItr != itrEnd )
		{
			do
			{
				auto& dim = *dimItr;
				( *outputItr ) = ( i_index / dimProduct ) % dim;
				dimProduct *= dim;

				++dimItr;
				++outputItr;
			} while( dimItr != itrEnd && outputItr != o_last );
		}

		( *outputItr ) = i_index / dimProduct;

		return true;
	}

	//--------------------------------
	/* end()
		get an iterator to one element past the end of the container.
	*/
	inline auto end()
	{
		assert( m_array.size() > 0 );
		return std::end( m_array );
	}

	//--------------------------------
	/* end() const
		get an iterator to one element past the end of the container.
	*/
	inline auto end() const
	{
		assert( m_array.size() > 0 );
		return std::cend( m_array );
	}

	//--------------------------------
	/* index_value_init()
		initialise the each value in the container to match its 1D index.
		This is useful for understanding the order of the elements / dimensions (row major order).
	*/
	void index_value_init()
	{
		for( size_t i = 0, c = size(); i < c; ++i )
		{
			m_array[i] = static_cast<data_t>( i );
		}
	}

	//--------------------------------
	/* Init( i_first, i_last )
		Initialise the container.
		Given two iterators containing the sizes of each dimension,
		setup the container to have those dimension and resize the internal container accordingly.

		NOTE: Called internally via constructors.
		NOTE: Can be called again later to re-purpose the container, eg from 2D to 3D.
	*/
	template<class itr_t, class = typename enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	inline void init( itr_t i_first, itr_t i_last )
	{
		assert( i_first != i_last );
		size_t const numElements = distance( i_first, i_last );

		if( numElements > 0 )
		{
			m_dim.clear();
			m_dim.reserve( numElements );

			size_t product = 1;
			do
			{
				m_dim.emplace_back( static_cast<size_t>( *i_first ) );
				product *= m_dim.back();	// count up the size of m_array at the same time
				++i_first;

				assert( m_dim.back() != data_t( 0 ) ); // not allowed a dimension of 0
			} while( i_first != i_last );

			assert( m_dim.size() == numElements );

			// allocate memory for array
			m_array.resize( product, data_t( 0 ) );
			return;
		}
		// else invalid construction
		assert( false );
	}

	//--------------------------------
	/* rbegin() const
		get iterator to first element.
	*/
	inline auto rbegin()
	{
		assert( m_array.size() > 0 );
		return std::rbegin( m_array );
	}

	//--------------------------------
	/* rbegin() const
		get iterator to first element.
	*/
	inline auto rbegin() const
	{
		assert( m_array.size() > 0 );
		return std::crbegin( m_array );
	}

	//--------------------------------
	/* rend() const
		get an iterator to one element past the end of the container.
	*/
	inline auto rend()
	{
		assert( m_array.size() > 0 );
		return std::rend( m_array );
	}

	//--------------------------------
	/* rend() const
		get an iterator to one element past the end of the container.
	*/
	inline auto rend() const
	{
		assert( m_array.size() > 0 );
		return std::crend( m_array );
	}

	//--------------------------------
	/* size() const
		get the total number of elements in the array.
	*/
	inline size_t size() const
	{
		assert( m_array.size() > 0 );
		return m_array.size();
	}

	//--------------------------------
	/* valid_index( i_first, i_last ) const
		returns true if the given multidimensional index is valid for this container.
	*/
	template<class itr_t, class = typename enable_if<sys_templates::is_iterator<itr_t> ::value, void>::type>
	bool valid_index( itr_t i_first, itr_t i_last ) const
	{
		if( static_cast<size_t>( distance( i_first, i_last ) ) != m_dim.size() )
		{
			return false;
		}

		auto itr = i_first;
		for( auto& e : m_dim )
		{
			if( *itr >= e )
			{
				return false;
			}
			++itr;
		}

		return true;
	}

private:
	//-------------------------------- Private members --------------------------------
	container_t m_array;
	dim_container_t m_dim;
};

#endif