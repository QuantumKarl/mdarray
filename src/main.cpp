/*	This file is part of MDArray.
	Copyright(C) 2016 by Karl Wesley Hutchinson

	MDArray is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	MDArray is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MDArray. If not, see <http://www.gnu.org/licenses/>.
*/

#define D_HAVE_BOOST 1

#include <iostream>
#include <random>
#include <limits>
#include <numeric>
using namespace std;

#include "MDArray.hpp"
#include "MDArrayTest.hpp"

int main()
{
	test1();
	test2(true,true,true,true);
	AlternativeIterationPatternsTest();
	test3<size_t>();
	test3<int>();
	test3<double>();
	test4();

	return 0;
}