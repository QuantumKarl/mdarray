/*	This file is part of MDArray.
	Copyright(C) 2016 by Karl Wesley Hutchinson

	MDArray is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	MDArray is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with MDArray. If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined (D_MDARRAY_TEST_HPP)
#define D_MDARRAY_TEST_HPP

#include <string.h> // for memmove

#include <vector>
using std::vector;

#if defined( D_HAVE_BOOST ) && (D_HAVE_BOOST == 1)
	#include <boost/pool/pool_alloc.hpp>
#endif

#include "short_alloc.hpp"

#if 1
	#define D_COUT_MD_TESTS
#endif

//--------------------------------
/* cycle( i_data, i_dataCount )
	moves along all elements by one,
	the element that falls off the end is put back to the front.
*/
template< typename T > // NOTE: POD only!
void cycle( T* i_data, size_t const i_dataCount ) // cycles by one
{
	// take temp copy of back
	T back = i_data[i_dataCount - 1];

	// move all the data along one (to the right, backward loop)
	memmove( &i_data[1], &i_data[0], sizeof( T )*( i_dataCount - 1 ) );

	// assign the back to the front
	i_data[0] = back;
}

//--------------------------------
/* test1()
	simple iteration, construction
*/
void test1()
{
	size_t x = 3, y = 3, z = 3, w = 3;
	MDArray<size_t> b2( { x,y } );
	b2.index_value_init();

	cout << "MDArray " << x << " by " << y << endl;
	for( size_t j = 0; j < y; ++j )
		for( size_t i = 0; i < x; ++i )
		{
		#if defined D_COUT_MD_TESTS
			cout << "[" << i << "," << j << "]" << " = " << (size_t)b2.at( { i,j } ) << endl;
		#endif
			b2.at( { i, j } ) = b2.at( { i,j } ) + 1;
		}
#if defined D_COUT_MD_TESTS
	b2.debug_cout();
#endif

	MDArray<size_t> bc( b2 );
#if defined D_COUT_MD_TESTS
	bc.debug_cout();
#endif
	vector<MDArray<size_t>> v;
	v.push_back( MDArray<size_t>( { x,y,z } ) );
	v.front() = MDArray<size_t>();

	cout << "MDArray " << x << " by " << y << " by " << z << endl;
	MDArray<size_t> b3( { x,y,z } );
	b3.index_value_init();

	size_t coord[4];
	for( size_t k = 0; k < z; ++k )
	{
		coord[2] = k;
		for( size_t j = 0; j < y; ++j )
		{
			coord[1] = j;
			for( size_t i = 0; i < x; ++i )
			{
				coord[0] = i;
			#if defined D_COUT_MD_TESTS
				cout << "[" << i << "," << j << "," << k << "]" << " = " << (size_t)b3.at( &coord[0], &coord[3] ) << endl;
			#endif
			}
		}
	}

	cout << "MDArray " << x << " by " << y << " by " << z << " by " << w << endl;
	MDArray<size_t> b4( { x,y,z,w } );
	b4.index_value_init();

	for( size_t ii = 0; ii < w; ++ii )
	{
		coord[3] = ii;
		for( size_t k = 0; k < z; ++k )
		{
			coord[2] = k;
			for( size_t j = 0; j < y; ++j )
			{
				coord[1] = j;
				for( size_t i = 0; i < x; ++i )
				{
					coord[0] = i;
				#if defined D_COUT_MD_TESTS
					cout << "[" << i << "," << j << "," << k << "," << ii << "]" << " = " << (size_t)b4.at( &coord[0], &coord[4] ) << endl;
				#endif
				}
			}
		}
	}
}

//--------------------------------
/* test2( i_2d, i_3d, i_4d, i_5d )
	1d to MD conversion
*/
void test2( bool const i_2d, bool const i_3d, bool const i_4d, bool const i_5d )
{
	// 2D test
	if( i_2d == true )
	{
		for( size_t y = 1; y < 4; ++y )
			for( size_t x = 1; x < 4; ++x )
			{
				MDArray<size_t> b2( { x,y } );
				b2.index_value_init();

				cout << "MDArray " << x << " by " << y << endl;
				size_t indexXY[2] = { 0,0 };
				size_t index = 0;
				for( size_t j = 0; j < y; ++j )
					for( size_t i = 0; i < x; ++i )
					{
						b2.encode_index( index, &indexXY[0], &indexXY[2] );
					#if defined D_COUT_MD_TESTS
						cout << "[" << i << "," << j << "]" << " = " << "[" << indexXY[0] << "," << indexXY[1] << "]" << " = " << index << endl;
					#endif
						++index;
						assert( b2.at( { i,j } ) == b2.at( { indexXY[0], indexXY[1] } ) );
					}
			}
	}

	size_t coord[5];
	size_t mdcoords[5];

	// 3D test
	if( i_3d == true )
	{
		//size_t x = 4, y = 3, z = 2;
		for( size_t z = 1; z < 4; ++z )
			for( size_t y = 1; y < 4; ++y )
				for( size_t x = 1; x < 4; ++x )
				{
					cout << "MDArray " << x << " by " << y << " by " << z << endl;
					MDArray<size_t> b3( { x,y,z } );
					b3.index_value_init();

					size_t index = 0;
					for( size_t k = 0; k < z; ++k )
					{
						coord[2] = k;
						for( size_t j = 0; j < y; ++j )
						{
							coord[1] = j;
							for( size_t i = 0; i < x; ++i )
							{
								coord[0] = i;

								b3.encode_index( index, &mdcoords[0], &mdcoords[3] );
							#if defined D_COUT_MD_TESTS
								cout << "[" << i << "," << j << "," << k << "]" << " = "
									<< "[" << mdcoords[0] << "," << mdcoords[1] << "," << mdcoords[2] << "]" << " = "
									<< index << " = " << (size_t)b3.at( &mdcoords[0], &mdcoords[3] ) << endl;
							#endif
								++index;
								assert( b3.at( &coord[0], &coord[3] ) == b3.at( &mdcoords[0], &mdcoords[3] ) );
							}
						}
					}
				}
	}

	// 4D test
	if( i_4d == true )
	{
		//size_t x = 3, y = 3, z = 3, w = 3;
		for( size_t w = 1; w < 4; ++w )
			for( size_t z = 1; z < 4; ++z )
				for( size_t y = 1; y < 4; ++y )
					for( size_t x = 1; x < 4; ++x )
					{
						cout << "MDArray " << x << " by " << y << " by " << z << " by " << w << endl;
						MDArray<size_t> b4( { x,y,z,w } );
						b4.index_value_init();

						size_t index = 0;
						for( size_t ii = 0; ii < w; ++ii )
						{
							coord[3] = ii;
							for( size_t k = 0; k < z; ++k )
							{
								coord[2] = k;
								for( size_t j = 0; j < y; ++j )
								{
									coord[1] = j;
									for( size_t i = 0; i < x; ++i )
									{
										coord[0] = i;

										b4.encode_index( index, &mdcoords[0], &mdcoords[4] );

									#if defined D_COUT_MD_TESTS
										cout << "[" << i << "," << j << "," << k << "," << ii << "]" << " = "
											<< "[" << mdcoords[0] << "," << mdcoords[1] << "," << mdcoords[2] << "," << mdcoords[3] << "]" << " = "
											<< index << " = " << (size_t)b4.at( &mdcoords[0], &mdcoords[4] ) << endl;
									#endif

										++index;
										assert( b4.at( &coord[0], &coord[4] ) == b4.at( &mdcoords[0], &mdcoords[4] ) );
									}
								}
							}
						}
					}
	}

	// 5D test
	if( i_5d == true )
	{
		for( size_t q = 1; q < 4; ++q )
			for( size_t w = 1; w < 4; ++w )
				for( size_t z = 1; z < 4; ++z )
					for( size_t y = 1; y < 4; ++y )
						for( size_t x = 1; x < 4; ++x )
						{
							cout << "MDArray " << x << " by " << y << " by " << z << " by " << w << " by " << q << endl;
							MDArray<size_t> b5( { x,y,z,w,q } );
							b5.index_value_init();

							auto current = b5.begin();
							auto end = b5.end();
							size_t index = 0;

							while( current != end )
							{
								b5.encode_index( index, &mdcoords[0], &mdcoords[5] );
								assert( b5.at( &mdcoords[0], &mdcoords[5] ) == *current );
								++current;
								++index;
							};
						}
	}
}

//--------------------------------
/* AlternativeIterationPatternsTest()

	NOTE: cycling ONLY supports square arrays!
*/
void AlternativeIterationPatternsTest()
{
	vector< vector<size_t> > points;
	points.resize( 5 );

	for( auto& e : points )
	{
		e.resize( 5 );
	}

# if 1
	// 2D test
	MDArray<size_t> b2( { 4,4 } );
	b2.index_value_init();

	for( size_t j = 0; j < 4; ++j ) // y
	{
		points[0][1] = j; // y
		for( size_t i = 0; i < 4; ++i ) // x
		{
			points[0][0] = i; // x

			// cpy normal iteration
			points[1] = points[0];

			// cycle by one, to get y iteration
			cycle( &points[1][0], 2 );

			size_t xIter = 0;
			b2.decode_index( &points[0][0], &points[0][2], xIter );
			size_t yIter = 0;
			b2.decode_index( &points[1][0], &points[1][2], yIter );

			cout << "xIter: [" << points[0][0] << "," << points[0][1] << "]" << endl;
			cout << "yIter: [" << points[1][0] << "," << points[1][1] << "]" << endl;
			cout << "xIterVal: " << (size_t)b2.at( xIter ) << endl;
			cout << "yIterVal: " << (size_t)b2.at( yIter ) << endl;
			i = i; // for break point
		}
		cout << endl;
	}
#endif

#if 1
	// 3D test
	for( size_t x = 2; x < 4; ++x )
	{
		size_t y, z;
		y = z = x;

		cout << "MDArray " << x << " by " << y << " by " << z << endl;
		size_t const dimCount = 3;
		MDArray<size_t> b3( { x,y,z } );
		b3.index_value_init();

		for( size_t i = 0, count = b3.size(); i < count; ++i )
		{
			// get x iteration
			b3.encode_index( i, &points[0][0], &points[0][dimCount] );

			// calculate all other dimensional iterations
			for( size_t d = 1; d < dimCount; ++d )
			{
				// cpy normal iteration
				points[d] = points[d - 1];

				// cycle by one, to get next dimensional iteration
				cycle( &points[d][0], dimCount );
			}

			// cout findings
			for( size_t d = 0; d < dimCount; ++d )
			{
				cout << "nIter: [";
				for( size_t dd = 0; dd < dimCount; ++dd )
				{
					cout << points[d][dd] << ",";
				}
				cout << "]" << " = " << (size_t)b3.at( &points[d][0], &points[d][dimCount] ) << endl;
			}
			cout << endl;
		}
	}

	// NOTES: X iteration increments on Y before Z as you would expect
	// NOTES: Y iteration increments on Z before X ie, at [0,2,0] it goes to, [0,0,1].
	// NOTES: Z iteration increments on X before Y ie, at [0,0,2] it goes to, [1,0,0].
#endif
}

//--------------------------------
/* test3()
	compilation test
*/
template< typename T >
void test3()
{
	vector<size_t> const dim = { 1, 2, 3, 4, 5 };

	{
		MDArray<T> d;// default ctor
		d.init( begin( dim ), end( dim ) );

		MDArray<T> i( begin( dim ), end( dim ) );// iterator ctor

		MDArray<T> il( { dim[0], dim[1], dim[2], dim[3] } );// init list

		MDArray<T> cpy( il );
		MDArray<T> mov( MDArray<T>( { dim[1],dim[2] } ) ); // cpy elison takes place
	}

	// lots of iterator things
	{
		MDArray<T> a1( begin( dim ), end( dim ) );// iterator ctor

		// the .begin and .end functions
		for( auto& e : a1 )
		{
			e = T( 1 );
		}

		// the const version of above
		T sum = T( 0 );
		for( auto const& e : a1 )
		{
			sum += e;
		}

		assert( static_cast<size_t>( sum ) == a1.size() );

		// call all variants of getting iterators
		{
			auto i1 = begin( a1 );
			auto i2 = end( a1 );
			auto const i3 = begin( a1 );
			auto const i4 = end( a1 );
			auto const i5 = cbegin( a1 );
			auto const i6 = cend( a1 );
			auto i7 = rbegin( a1 );
			auto i8 = rend( a1 );
			auto const i9 = rbegin( a1 );
			auto const i10 = rend( a1 );
			auto const i11 = crbegin( a1 );
			auto const i12 = crend( a1 );
			assert( i1 == i3 );
			assert( i2 == i4 && i6 == i4 );
			assert( i7 == i9 && i9 == i11 );
			assert( i8 == i10 && i12 == i10 );
		}
		{
			auto i1 = a1.begin();
			auto i2 = a1.end();
			auto const i3 = a1.begin();
			auto const i4 = a1.end();
			auto const i5 = a1.cbegin();
			auto const i6 = a1.cend();
			auto i7 = a1.rbegin();
			auto i8 = a1.rend();
			auto const i9 = a1.rbegin();
			auto const i10 = a1.rend();
			auto const i11 = a1.crbegin();
			auto const i12 = a1.crend();
			assert( i1 == i3 );
			assert( i2 == i4 && i6 == i4 );
			assert( i7 == i9 && i9 == i11 );
			assert( i8 == i10 && i12 == i10 );
		}
	}

	{
		MDArray<T> a1( begin( dim ), end( dim ) );// iterator ctor
		bool val1 = a1.at( 0 ) == *a1.begin();

		vector<size_t> coord( 5, 0 );
		bool val2 = a1.at( { 0,0,0,0,0 } ) == a1.at( begin( coord ), end( coord ) );

		a1.clear();

		assert( val1 && val2 );

		if( val1 && val2 )
		{
			cout << endl;
		}
	}

	{
		MDArray<T> a1( begin( dim ), end( dim ) );// iterator ctor
		vector<size_t> expcetedDim( begin( dim ), end( dim ) );

		auto const& t = a1.dimensions();

		auto itr = begin( expcetedDim );
		for( auto& e : t )
		{
			if( e != *itr )
			{
				cout << "FAILED TEST" << endl;
				return;
			}
			*itr -= 1; // setup next test
			++itr;
		}

		expcetedDim.back() += 2; // make last index invalid
		bool val = a1.valid_index( begin( expcetedDim ), end( expcetedDim ) );
		assert( val == false );

		if( val == false )
		{
			cout << endl;
		}
	}

	{
		MDArray<T> a1;
		MDArray<T> a2;
		a1 = a2 = MDArray<T>( begin( dim ), end( dim ) );
		auto const& dim1 = a1.dimensions();
		auto const& dim2 = a2.dimensions();

		auto itr = begin( dim2 );
		for( auto& e : dim1 )
		{
			if( e != *itr )
			{
				cout << "FAILED TEST" << endl;
				return;
			}
			++itr;
		}
	}
}

//----------------
/* StackArray
	Template used in the below test,
	defines an allocator which is stack based.
*/
template < size_t bufSize1 = sizeof( size_t ) * 27, size_t bufSize2 = sizeof( size_t ) * 3 >
using StackArray = MDArray<size_t, short_alloc<size_t, bufSize1, alignof( size_t )>, size_t, short_alloc<size_t, bufSize2, alignof( size_t )> >;

//--------------------------------
/* test4()
	boost pool allocator and
	custom allocator
*/
void test4()
{
#if defined( D_HAVE_BOOST ) && (D_HAVE_BOOST == 1)
	{
		//--------------------------------
		/* boost source / reference
		http://www.boost.org/doc/libs/1_61_0/libs/pool/doc/html/boost/pool_allocator.html
		..\boost_1_61_0\libs\pool\example */

		typedef boost::pool_allocator<double> alloc1;
		typedef boost::pool_allocator<size_t> alloc2;

		MDArray< double, alloc1, size_t, alloc2 > arr( { 19,19,19 } );

		double acc = 1.0;
		for( auto& e : arr )
		{
			acc += sqrt( acc );
			e = acc;
			//cout << e << endl;
		}

	#if 0
		boost::singleton_pool<boost::pool_allocator_tag, sizeof( double )>::purge_memory();
		boost::singleton_pool<boost::pool_allocator_tag, sizeof( size_t )>::purge_memory();
	#endif
	}
#endif

	{
		StackArray<>::data_allocator_type::arena_type a;
		StackArray<>::dimension_allocator_type::arena_type b;
		StackArray<> arr( a, b );
		vector<size_t> dims( 3, 3 );
		arr.init( begin( dims ), end( dims ) );
		arr.index_value_init();

		for( auto& e : arr )
		{
			cout << e << endl;
		}
	}
}
#endif